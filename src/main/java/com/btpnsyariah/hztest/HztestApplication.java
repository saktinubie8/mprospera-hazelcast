package com.btpnsyariah.hztest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HztestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HztestApplication.class, args);
	}
}
